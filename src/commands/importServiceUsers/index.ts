import { getClient } from '@api/client';
import commandLineArgs from 'command-line-args';
import commandLineUsage from 'command-line-usage';
import { parse } from 'csv-parse/sync';
import fs from 'fs';
import { ExternalUserDetail, UserDetails } from './types';
import { z } from 'zod';

const definitions = [{ name: 'file' }, { name: 'service' }];
const sections = [
  {
    header: 'Options',
    optionList: [
      {
        name: 'file',
        description: 'The file we will convert. Must be CSV',
        require: true,
      },
      {
        name: 'service',
        description: 'The service id where we will import the users',
        require: true,
      },
    ],
  },
];

const exitWithError = (message: string) => {
  console.error(message);
  console.error(commandLineUsage(sections));
  process.exit(1);
};

export default async function (argv: string[]) {
  const runOptions = commandLineArgs(definitions, {
    argv,
    stopAtFirstUnknown: true,
  });

  if (!runOptions.file) {
    exitWithError('CSV file must be specified');
  }
  if (!runOptions.service) {
    exitWithError('Service ID must be specified');
  }

  console.log('~~~ Import user data ~~~');
  const serviceId = runOptions.service;
  const records = parseCsv(runOptions.file);

  console.log('🔵 No. Records', records.length);

  const client = await getClient(runOptions.env);
  if (!client) {
    console.error('No client found');
    return;
  }
  const savedRecords: string[] = [];
  const linesToSave: string[] = [];
  for (const record of records) {
    const index = records.indexOf(record);

    const {
      email,
      name,
      phone,
      registrationNumber,
      year,
      brand,
      capacity,
      fuel,
      model,
      power,
      vin,
    } = formatUserData(record);
    const uniqueKey = `${serviceId}-${registrationNumber}-${vin}`;
    if (savedRecords.includes(uniqueKey)) {
      continue;
    } else {
      savedRecords.push(uniqueKey);
    }
    const line = `($$${email || ''}$$, $$${name || ''}$$, $$${
      phone || ''
    }$$, $$${registrationNumber || ''}$$, $$${serviceId || ''}$$, $$${
      year || ''
    }$$, $$${brand || ''}$$, $$${capacity || ''}$$, $$${fuel || ''}$$, $$${
      model || ''
    }$$, $$${power || ''}$$, $$${vin || ''}$$)`;
    if (index % 100 === 0) {
      console.log('🟢 Saving', index, 'of', records.length);
    }
    linesToSave.push(line);
  }
  const getUserQuery = `
      INSERT INTO externalUserDetails ("email", "name", "phone", "registrationNumber", "service", "year", "brand", "capacity", "fuel", "model", "power", "vin")
      VALUES ${linesToSave.join(', ')};
    `;

  try {
    await client.query(getUserQuery);
  } catch (error: any) {
    console.log('🔴 Error inserting the items', error.message);
  }
  console.log('~~~ Import user data END ~~~');
  return;
}

const formatUserData = (record: ExternalUserDetail): Partial<UserDetails> => {
  const validNumber = (number: string) => {
    const formattedNumber = number && number.replace(/\./g, '');
    const regex = /^\d{5,20}$/; // Matches a number with 5 to 20 digits
    if (regex.test(formattedNumber)) {
      return formattedNumber;
    }
    return null;
  };
  const number =
    validNumber(record.phone) ||
    validNumber(record.phone2) ||
    validNumber(record.phone3) ||
    validNumber(record.phone4) ||
    '';
  const registrationNumber = record.registrationNumber
    .replace(/\s/g, '')
    .replace(/[^0-9a-zA-Z]/g, '')
    .toUpperCase();

  const vin = record.vin.toUpperCase();
  const emailData = z.string().email().safeParse(record.email);
  return {
    ...record,
    name: [record.fname, record.lname].filter(Boolean).join(' '),
    phone: number,
    registrationNumber,
    vin,
    email: emailData.success ? emailData.data : '',
    model: record.model.replace(`${record.brand} `, ''),
  };
};

const parseCsv = (file: string): ExternalUserDetail[] => {
  const csv = fs.readFileSync(file);

  // Initialize the parser
  const records: string[][] = parse(csv, {
    delimiter: ',',
  });

  const headers = records[0];
  const data = records
    .map((record, index) => {
      if (index === 0) {
        return null;
      }
      const obj: any = {};
      headers.forEach((header, index) => {
        obj[header] = record[index];
      });
      return obj;
    })
    .filter(Boolean);

  return data;
};
