import z from 'zod';

export const externalUserDetailSchema = z.object({
  fname: z.string(),
  lname: z.string(),
  phone: z.string(),
  phone2: z.string(),
  phone3: z.string(),
  phone4: z.string(),
  email: z.string(),
  data_inchidere: z.string(),
  vin: z.string(),
  registrationNumber: z.string(),
  brand: z.string(),
  model: z.string(),
});
export type ExternalUserDetail = z.infer<typeof externalUserDetailSchema>;

export const userDetailsSchema = z.object({
  email: z.string(),
  name: z.string(),
  phone: z.string(),
  registrationNumber: z.string(),
  service: z.string(),
  year: z.string(),
  brand: z.string(),
  capacity: z.string(),
  fuel: z.string(),
  model: z.string(),
  power: z.string(),
  vin: z.string(),
});
export type UserDetails = z.infer<typeof userDetailsSchema>;
